%global qt_module qtdatavis3d

Summary: 	Qt5 - Qt Data Visualization component
Name:    	qt5-%{qt_module}
Version: 	5.15.10
Release: 	1

# See LGPL_EXCEPTIONS.txt, LICENSE.GPL3, respectively, for exception details
License: LGPL-3.0-only OR GPL-3.0-only WITH Qt-GPL-exception-1.0
Url:     http://www.qt.io
%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://download.qt.io/official_releases/qt/%{majmin}/%{version}/submodules/%{qt_module}-everywhere-opensource-src-%{version}.tar.xz

BuildRequires:  make
BuildRequires:  qt5-qtbase-devel >= %{version}
BuildRequires:  qt5-qtdeclarative-devel >= %{version}

%description
Qt Data Visualization module provides multiple graph types to visualize data in
3D space both with C++ and Qt Quick 2.

%package devel
Summary: 	Development files for %{name}
Requires: 	%{name}%{?_isa} = %{version}-%{release}
Requires: 	qt5-qtbase-devel%{?_isa}
Requires: 	qt5-qtdeclarative-devel%{?_isa}
%description devel
%{summary}.

%package examples
Summary: 	Programming examples for %{name}
Requires: 	%{name}%{?_isa} = %{version}-%{release}
%description examples
%{summary}.


%prep
%setup -q -n %{qt_module}-everywhere-src-%{version}


%build
mkdir %{_target_platform}
pushd %{_target_platform}
%{qmake_qt5} ..
popd

%make_build -C %{_target_platform}


%install
make install INSTALL_ROOT=%{buildroot} -C %{_target_platform}


%ldconfig_scriptlets

%files
%license LICENSE.GPL3
%{_qt5_libdir}/libQt5DataVisualization.so.5*
%{_qt5_qmldir}/QtDataVisualization/

%files devel
%{_qt5_headerdir}/QtDataVisualization/
%{_qt5_libdir}/libQt5DataVisualization.so
%{_qt5_libdir}/libQt5DataVisualization.prl
%{_qt5_libdir}/pkgconfig/Qt5DataVisualization.pc
%{_qt5_libdir}/cmake/Qt5DataVisualization/
%{_qt5_archdatadir}/mkspecs/modules/*
%exclude %{_qt5_libdir}/libQt5DataVisualization.la

%if 0%{?_qt5_examplesdir:1}
%files examples
%{_qt5_examplesdir}/
%endif


%changelog
* Mon Aug 21 2023 huayadong <huayadong@kylinos.cn> - 5.15.10-1
- update to version 5.15.10-1

* Tue Jun 28 2022 tanyulong <tanyulong@kylinos.cn> - 5.15.2-1
- init package for openEuler

